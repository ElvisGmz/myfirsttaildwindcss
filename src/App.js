import Navbar from './Components/Navbar'
import Hero from './Components/Hero'
import Content from './Components/Content'

function App() {
  return (
    <div className="App"> 
      <Navbar />
      <Hero />
      <Content />
    </div>
  );
}

export default App;
