import React from "react";

export default function Navbar() {
  return (
    <header className="w-full">
      <nav className="flex justify-between items-center w-full max-w-screen-xl m-auto py-3">
        <div className="text-xl font-bold">FlyHight</div>
        <div className="flex justify-between font-semibold w-3/12 underline-hover">
          <a href="">Destination</a>
          <a href="">Bookings</a>
          <a href="">Activities</a>
        </div>
        <div id="login" className="flex text-sm justify-between items-center w-32 underline-hover">
          <a href="">Log In</a>
          <a href="" className="bg-black hover:bg-blue-500 text-white p-2 rounded-md">Sign Up</a>
        </div>
      </nav>
    </header>
  );
}
