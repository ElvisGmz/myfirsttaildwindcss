import React from "react";

export default function Hero() {
  return (
    <>
      <div className="relative mt-4 w-full max-w-screen-xl max-h m-auto rounded-3xl overflow-hidden">
        <img
          className="object-cover"
          src="https://c.wallhere.com/photos/f7/49/1600x1200_px_3d_Blue_Background_building_Cityscape_digital_art_Lights_minimalism-995630.jpg!d"
          alt=""
        />
        <div className="absolute top-20 text-xl left-20 text-white">
          <p>- Blue Mountain Country Club and Resort</p>
          <h1 className="text-8xl font-bold mb-20 mt-5">Treebo Tryst</h1>
          <p className="font-semibold text-xl">- 02°C Very Cold</p>
        </div>
      </div>

      <div className="relative flex items justify-evenly w-8/12 bg-white -mt-12 rounded-2xl py-8 mx-auto text-xl shadow-2xl">
        <div>
            <h1>Location</h1>
            <p>Lorem ipsum dolor sit</p>
        </div>
        <div>
            <h1>Activity</h1>
            <p>Lorem ipsum dolor sit</p>
        </div>
        <div>
            <h1>Date</h1>
            <p>Lorem ipsum dolor sit</p>
        </div>
        <div>
            <h1>Open</h1>
            <p>Lorem ipsum dolor sit</p>
        </div>
      </div>
    </>
  );
}
