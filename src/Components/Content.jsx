import React from 'react'

export default function Content() {
    return (
        <div className="mx-auto mt-10 w-full max-w-screen-xl">
            <h1 className="font-bold text-3xl mb-4">Trending 2020</h1>
            <div className="flex cards flex-wrap justify-center items-stretch">
                <div>
                    <img src="https://images-na.ssl-images-amazon.com/images/S/pv-target-images/81311fc1c757b6e9919bed22ba4c01521210b39dec19210789c6aba09f705fb7._RI_V_TTW_.jpg" alt=""/>
                    <div>
                    <h1>SpiderVerse Movie</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo dolore rerum ullam itaque ea provident dolorem magnam animi laborum perferendis sunt quam fuga nesciunt ut voluptas quia earum, iure assumenda!</p>
                    </div>
                </div>
                <div>
                    <img src="https://images-na.ssl-images-amazon.com/images/S/pv-target-images/81311fc1c757b6e9919bed22ba4c01521210b39dec19210789c6aba09f705fb7._RI_V_TTW_.jpg" alt=""/>
                    <div>
                    <h1>SpiderVerse Movie</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo dolore rerum ullam itaque ea provident dolorem magnam animi laborum perferendis sunt quam fuga nesciunt ut voluptas quia earum, iure assumenda!</p>
                    </div>
                </div>
                <div>
                    <img src="https://images-na.ssl-images-amazon.com/images/S/pv-target-images/81311fc1c757b6e9919bed22ba4c01521210b39dec19210789c6aba09f705fb7._RI_V_TTW_.jpg" alt=""/>
                    <div>
                    <h1>SpiderVerse Movie</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo dolore rerum ullam itaque ea provident dolorem magnam animi laborum perferendis sunt quam fuga nesciunt ut voluptas quia earum, iure assumenda!</p>
                    </div>
                </div>
                <div>
                    <img src="https://images-na.ssl-images-amazon.com/images/S/pv-target-images/81311fc1c757b6e9919bed22ba4c01521210b39dec19210789c6aba09f705fb7._RI_V_TTW_.jpg" alt=""/>
                    <div>
                    <h1>SpiderVerse Movie</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo dolore rerum ullam itaque ea provident dolorem magnam animi laborum perferendis sunt quam fuga nesciunt ut voluptas quia earum, iure assumenda!</p>
                    </div>
                </div>
                <div>
                    <img src="https://images-na.ssl-images-amazon.com/images/S/pv-target-images/81311fc1c757b6e9919bed22ba4c01521210b39dec19210789c6aba09f705fb7._RI_V_TTW_.jpg" alt=""/>
                    <div>
                    <h1>SpiderVerse Movie</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo dolore rerum ullam itaque ea provident dolorem magnam animi laborum perferendis sunt quam fuga nesciunt ut voluptas quia earum, iure assumenda!</p>
                    </div>
                </div>
                <div>
                    <img src="https://images-na.ssl-images-amazon.com/images/S/pv-target-images/81311fc1c757b6e9919bed22ba4c01521210b39dec19210789c6aba09f705fb7._RI_V_TTW_.jpg" alt=""/>
                    <div>
                    <h1>SpiderVerse Movie</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo dolore rerum ullam itaque ea provident dolorem magnam animi laborum perferendis sunt quam fuga nesciunt ut voluptas quia earum, iure assumenda!</p>
                    </div>
                </div>
                <div>
                    <img src="https://images-na.ssl-images-amazon.com/images/S/pv-target-images/81311fc1c757b6e9919bed22ba4c01521210b39dec19210789c6aba09f705fb7._RI_V_TTW_.jpg" alt=""/>
                    <div>
                    <h1>SpiderVerse Movie</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo dolore rerum ullam itaque ea provident dolorem magnam animi laborum perferendis sunt quam fuga nesciunt ut voluptas quia earum, iure assumenda!</p>
                    </div>
                </div>
                <div>
                    <img src="https://images-na.ssl-images-amazon.com/images/S/pv-target-images/81311fc1c757b6e9919bed22ba4c01521210b39dec19210789c6aba09f705fb7._RI_V_TTW_.jpg" alt=""/>
                    <div>
                    <h1>SpiderVerse Movie</h1>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Explicabo dolore rerum ullam itaque ea provident dolorem magnam animi laborum perferendis sunt quam fuga nesciunt ut voluptas quia earum, iure assumenda!</p>
                    </div>
                </div>
            </div>
        </div>
    )
}
